# GitLab - CICD Infrastructure

[[_TOC_]]

## Description

[Terraform](https://www.terraform.io/) state for configuring and managing resources related to CICD
within the context of GitLab.

This mainly contains configurations for Auth methods that the GitLab Runners and Executor will use
along with Vault & Nomad policies with the required permissions for deployments. 

## Other info

Policies needed for CICD jobs (as in JWT generated from GitLab itself) are defined in the
[Repository Management](https://gitlab.com/carboncollins-cloud/repository-management) repository.

## Other Links

- [GitLab CI/CD - Job JWT Auth with Vault](https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/)
- [Vault - JWT Auth](https://www.vaultproject.io/docs/auth/jwt)
- [Vault - AppRole Auth](https://www.vaultproject.io/docs/auth/approle)
- [Vault - Nomad Secrets Backend](https://www.vaultproject.io/docs/secrets/nomad)
- [GitLab Runner - Base Container](https://gitlab.com/carboncollins-cloud/cicd/gitlab-runner-container)
- [GitLab Executor - Base Container](https://gitlab.com/carboncollins-cloud/cicd/gitlab-executor-container)
- [Repository Management](https://gitlab.com/carboncollins-cloud/repository-management)
