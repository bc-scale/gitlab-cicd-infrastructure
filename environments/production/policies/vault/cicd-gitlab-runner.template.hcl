path "auth/token/lookup-self" {
  capabilities = ["read"]
}

path "auth/token/renew-self" {
  policy = "write"
}

path "${nomad_secret_backend_path}/creds/${nomad_credential_name}" {
  capabilities = ["read"]
}
